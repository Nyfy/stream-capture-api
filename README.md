# Stream Capture Service
## Introduction
A simple microservice that sources video files from popular platforms such as Youtube, Twitch, Facebook, Instagram, TED and more. If a suitable source is found, the file is uploaded directly to Amazon S3.

## Getting Started
Clone the repository:
```
git clone https://gitlab.com/Nyfy/stream-capture-api.git
```
Create your config.ini:
```
cd stream-capture-api
cp ./config.ini.example ./config.ini
```
Configure config.ini with the appropriate values and AWS token

Build and run the docker container:
```
cd stream-capture-api
docker build -t stream-capture-service .
docker run -p 5000:5000 stream-capture-service
```

Send a request:
```
curl localhost:5000/capture?url=https://www.youtube.com/watch?v=at_f98qOGY0
```

Find the file in S3!
