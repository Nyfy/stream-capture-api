import boto3, json, logging, cerberus, uuid, configparser

from handlers.capture import Capture
from flask import Flask, request, Response
from logging.handlers import RotatingFileHandler

###   ____  _____ _____ ____  
###  |  _ \| ____|  ___/ ___| 
###  | | | |  _| | |_  \___ \ 
###  | |_| | |___|  _|  ___) |
###  |____/|_____|_|   |____/ 
###

# Setup config.ini parsing and getter function
config = configparser.ConfigParser()
config.read('config.ini')

def get_config(key):
    return config['DEFAULT'][key]

# Initialize flask logging with a rotating file handler
def initialize_logging(app):
    level = logging.DEBUG if get_config('LOG_LEVEL') == 'DEBUG' else logging.INFO

    app.logger.setLevel(level)

    handler = RotatingFileHandler('stream-capture.log', maxBytes=250000, backupCount=5)
    handler.setLevel(level)

    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)

    app.logger.addHandler(handler)

###  __  __    _    ___ _   _ 
### |  \/  |  / \  |_ _| \ | |
### | |\/| | / _ \  | ||  \| |
### | |  | |/ ___ \ | || |\  |
### |_|  |_/_/   \_\___|_| \_|
###

# Flask
app = Flask(__name__)
initialize_logging(app)

# Amazon S3
s3 = boto3.resource('s3', 
                    region_name=get_config('S3_BUCKET_REGION'),
                    aws_access_key_id=get_config('AWS_ACCESS_KEY_ID'),
                    aws_secret_access_key=get_config('AWS_SECRET_ACCESS_KEY'))


###  ____   ___  _   _ _____ _____ ____  
### |  _ \ / _ \| | | |_   _| ____/ ___| 
### | |_) | | | | | | | | | |  _| \___ \ 
### |  _ <| |_| | |_| | | | | |___ ___) |
### |_| \_\\___/ \___/  |_| |_____|____/ 
###

@app.route('/capture')
def capture():
    try:
        request_id = uuid.uuid4()

        capture_schema = {
                    'url': {'type': 'string'}
                 }

        capture_validator = cerberus.Validator(capture_schema, require_all=True)

        # Validate the request
        if not capture_validator.validate(request.args):
            app.logger.error('[%s] Request failed validation: %s', request_id, capture_validator.errors)
            return Response(json.dumps({'type': 'validation error', 'message': capture_validator.errors})), 400

        # Handle the request
        return Capture(app, config, request_id).handle_request(s3, request.args['url'])
    
    except Exception as e:
        app.logger.error('[%s] ' + 'Uncaught error occured while handling request' + ': %s', request_id, e)
        return Response(json.dumps({'type': 'error', 'message': str(e)})), 500

if __name__ == '__main__':
    app.run(host=get_config('HOST'), port=get_config('PORT'))