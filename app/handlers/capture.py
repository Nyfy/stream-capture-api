import requests, re, urllib, json, os

from flask import Response

class Capture:
    def __init__(self, app, config, request_id):
        self.app = app
        self.config = config
        self.request_id = request_id

    def get_config(self, key):
        return self.config['DEFAULT'][key]

    # Get the S3 compatible filename
    def get_s3_filename(self, video_title, extension):
        return re.sub('[^\w\d!\-_\.\*,()]', '', video_title) + '.' + extension

    # Get video source information for the requested URL from https://download-api.com
    def get_video_source(self, video_url):

        headers = {
            'X-RapidAPI-Host': self.get_config('RAPID_API_HOST'),
            'X-RapidAPI-Key': self.get_config('RAPID_API_KEY'),
            'content-type': 'application/json'
        }

        raw_content = requests.get('https://getvideo.p.rapidapi.com/?url=' + urllib.parse.quote(video_url), headers=headers)._content.decode('utf-8')

        return json.loads(urllib.parse.unquote(raw_content))

    # Find the best stream from the various available streams
    def get_quality_stream(self, streams):
        # Find one thats HD, or else just default to the first entry (its generally the highest quality anyways)
        for stream in streams:
            if stream['has_audio'] and stream['has_video'] and stream['is_hd']:
                return stream
    
        return streams[0]
    
    def handle_request(self, s3, request_url):
        try:
            self.app.logger.info('[%s] Request received for video at %s', self.request_id, request_url)

            video_source = self.get_video_source(request_url)
            download_stream = self.get_quality_stream(video_source['streams'])

            if download_stream == None:
                message = 'No download stream for ' + request_url + ' found'
                self.app.logger.error('[%s] ' + message, self.request_id)
                return Response(json.dumps({'type': 'resource error', 'message': message})), 404
            else :
                self.app.logger.debug('[%s] Received download stream from video source API: %s', self.request_id, download_stream)

            # Get the video
            self.app.logger.info('[%s] Downloading video from source %s', self.request_id, download_stream['url'])
            video = requests.get(download_stream['url'], allow_redirects=True)

            # Write to file
            filename = self.get_s3_filename(video_source['title'], download_stream['extension'])
            self.app.logger.info('[%s] Writing to file %s', self.request_id, filename)
            open(filename, 'wb').write(video.content)

            bucket_name = 'stream-capture-' + str(video_source['site']).lower()

            # Setup/verify S3 bucket
            if s3.Bucket(bucket_name) not in s3.buckets.all():
                self.app.logger.warn('[%s] Bucket %s not found or not accessible, attempting to create it now.', self.request_id, bucket_name)

                response = s3.meta.client.create_bucket(
                    Bucket = bucket_name,
                    CreateBucketConfiguration={
                        'LocationConstraint': self.get_config('S3_BUCKET_REGION')
                    }
                )

                if response['ResponseMetadata']['HTTPStatusCode'] != 200:
                    self.app.logger.error('[%s] Error occured creating bucket %s (%s) - aborting operation', self.request_id, bucket_name, response)
                    os.remove(filename)
                    return Response(json.dumps({'type': 'aws error', 'message': 'An error occurred during an AWS operation.'})), 502

            # Upload file to S3
            self.app.logger.info('[%s] Starting upload of file %s to bucket %s', self.request_id, filename, bucket_name)
            
            response = s3.meta.client.upload_file(filename, bucket_name, filename)
            if response and response['ResponseMetadata']['HTTPStatusCode'] != 200:
                self.app.logger.error('[%s] Error occured uploading file to S3 (%s)- aborting operation', self.request_id, response)
                os.remove(filename)
                return Response(json.dumps({'type': 'aws error', 'message': 'An error occurred during an AWS operation.'})), 502

            self.app.logger.info('[%s] Successfully captured stream %s as %s to S3', self.request_id, request_url, filename)

            os.remove(filename)
            return Response(json.dumps({'type': 'successful operation', 'message': filename})), 200
        except Exception as e: 
            self.app.logger.error('[%s] Uncaught error occured while fulfilling request: %s', self.request_id, e)
            return Response(json.dumps({'type': 'error', 'message': e})), 500